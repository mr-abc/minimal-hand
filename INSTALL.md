# minimal-hand: [Monocular Real-time Hand Shape and Motion Capture using Multi-modal Data](https://arxiv.org/abs/2003.09572)
- tensorflow: https://github.com/CalciferZh/minimal-hand
- pytorch: https://github.com/MengHao666/Minimal-Hand-pytorch

This repo moves the above pytorch repo to python 3.10.

## Create environment
```
conda create -n minimal-hand python=3.10
conda activate minimal-hand
```

## Train `ShapeNet`

Refer to [ShapeNet.md](ShapeNet.md)

### Run `python create_data.py`
```
pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
pip install git+https://github.com/hassony2/manopth.git
pip install chumpy
pip install numpy==1.23
pip install python-opencv
pip install tqdm
```

### Run `train_shape_net.py`
```
pip install progress
pip install tensorboardX
pip install termcolor
pip install matplotlib
```
spend **1.5h** on my desktop: DELL Inspiration 57700, output `checkpoints/ckp_siknet_synth_151.pth.tar`

## Run the demos

Refer to [README.md](README.md)

### Intasll requirements
```
pip install pandas
pip install einops
pip install open3d
pip install transforms3d
```

### Download the [mano models](https://mano.is.tue.mpg.de/)
```
mano/
    models/
        MANO_LEFT.pkl
        MANO_RIGHT.pkl
        SMPLH_female.pkl
        SMPLH_male.pkl
``` 
**Don't copy `webuser` directory, because `mano` is installed when `pip install git+https://github.com/hassony2/manopth.git`**

### Download the trained model from the author
- [Google Drive](https://drive.google.com/file/d/1e6aG4ZSOB6Ri_1TjXI9N-1r7MtwmjA6w/view?usp=sharing) or 
  [Baidu Pan](https://pan.baidu.com/s/1Hh0ZU8p04prFVSp9bQm_IA) (`2rv7`)
- extract it into the `checkpoints` directory
```
checkpoints/
    ckp_denet_106.pth
    ckp_siknet_synth_151.pth.tar
```

## Train `DetNet`

Refer to [README.md](README.md)

### Download `RHD`
[Rendered Handpose Dataset](https://lmb.informatik.uni-freiburg.de/resources/datasets/RenderedHandposeDataset.en.html)
```
data/

    RHD/
        RHD_published_v2/
            evaluation/
            training/
            view_sample.py
            ...        
```

### `GLIBCXX_3.4.29' not found
> ImportError: /lib/x86_64-linux-gnu/libstdc++.so.6: version `GLIBCXX_3.4.29' not found (required by /home/lebei/anaconda3/envs/minimal-hand/lib/python3.10/site-packages/google/protobuf/pyext/_message.cpython-310-x86_64-linux-gnu.so)

Install g++ 11 on Ubuntu 20.04

```
sudo add-apt-repository -y ppa:ubuntu-toolchain-r/test
sudo apt update
sudo apt install -y g++-11
```

### Run the training code
```
python train_detnet.py --data_root data/ --datasets_train rhd --datasets_test rhd --train_batch 16
```

spend **2.5h** for one epoch on my desktop: DELL Inspiration 57700

**Errors after one epoch**

> torch.cuda.OutOfMemoryError: CUDA out of memory. Tried to allocate 512.00 MiB (GPU 0; 3.95 GiB total capacity; 1.71 GiB already allocated; 348.19 MiB free; 2.94 GiB reserved in total by PyTorch) If reserved memory is >> allocated memory try setting max_split_size_mb to avoid fragmentation.  See documentation for Memory Management and PYTORCH_CUDA_ALLOC_CONF

### Detnet training and evaluation curve from the author

```
python plot.py --out_path out_loss_auc
```

## View RHD dataset

```
conda create -n rhd_view python=2.7
conda activate rhd_view
pip install scipy==1.1.0
pip install Pillow
cd data/RHD/RHD_published_v2
python view_samples.py
```